# -*- coding: utf-8 -*-

import os
import sys
sys.path.append("..")
sys.path.append("../util")
sys.path.append("../param")

import util.wordsplit as sp
import src.ngram.ngram as ng

import param.common_param as param 
import src.ngram.ngram_param as ng_param


def tune_test(ngram_app):
    print("Start training......\n")
    ngram_app.train(1)

    #ngram_app.show()

    #tune test
    path = param.DATAPATH+"/tune_test"
    sum_count = 0
    acc_count = 0
    for i in range(len(param.LANGNAMES)):
        lang_vect = []
        files = os.listdir(path+"/"+param.LANGNAMES[i])
        for f in files:
            sum_count = sum_count + 1
            if not os.path.isdir(f):
                lang_vect = lang_vect + sp.split(path+"/"+param.LANGNAMES[i]+"/"+f, param.LANGNAMES[i], 4);
                if(param.LANGNAMES[i] == ngram_app.infer(lang_vect)):
                    acc_count = acc_count + 1

    accuracy = acc_count / float(sum_count)
    return accuracy


#######################TUNE_TEST########################
print("Testing to tune K and N for ngram")
multi_vector = {}

for i in range(len(param.LANGNAMES)):
    lang_vect = []
    files = os.listdir(param.DATAPATH+"/"+param.LANGNAMES[i])
    file_count = 0
    print("Collecting "+param.LANGNAMES[i]+"'s data......\n")

    for f in files:

        file_count = file_count + 1
        print("Start spliting file #"+str(file_count)+"of language"+param.LANGNAMES[i]+"\n")

        lang_vect = lang_vect + sp.split(param.DATAPATH+"/"+param.LANGNAMES[i]+"/"+f, param.LANGNAMES[i], 4);

    multi_vector[param.LANGNAMES[i]] = lang_vect

ngram_app = ng.ngram_approach(multi_vector)
fN = 3 
fK = 300 

#accuracy_rate = 0
#for N in range(3,6):
#    ng_param.N = N
#    for K in range(300, 500, 50):
#        ng_param.K = K
#        acc = tune_test(ngram_app) 
#        print(acc)
#        if(acc > accuracy_rate):
#            print("before: "+str(accuracy_rate)+" "+"after: "+str(acc))
#            accuracy_rate = acc
#            fN = N
#            fk = K
#            print("N is: "+str(fN)+",K is: "+str(fK))
            
#ngram_app.save_model("../models/ngram_model")
######################INFER_TEST########################
#Through parameter searching. when N = 5; k = 300; accuracy = 91%
ng_param.N = 5
ng_param.K = 300 

print("Start training......\n")
ngram_app.train(1)

print("Start infer test:\n")

path = param.DATAPATH+"/test"
sum_count = 0
acc_count = 0

for i in range(len(param.LANGNAMES)):
    lang_vect = []
    files = os.listdir(path+"/"+param.LANGNAMES[i])
    for f in files:
        sum_count = sum_count + 1
        if not os.path.isdir(f):
            lang_vect = lang_vect + sp.split(path+"/"+param.LANGNAMES[i]+"/"+f, param.LANGNAMES[i], 4);
            if(param.LANGNAMES[i] == ngram_app.infer(lang_vect)):
                acc_count = acc_count + 1

accuracy = acc_count / float(sum_count)
print(str(fN)+" "+str(fK)+"\n")
print("ACCURACY is: %.3f" % accuracy)
